import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mobile_scanner/mobile_scanner.dart';

import '../db/db_helper.dart';
import '../home/CardDetailPage.dart';
import '../main.dart';
import 'qr_overlay.dart';

class CodeScanner extends StatefulWidget {
  final String cardName;

  CodeScanner({required this.cardName});

  @override
  _CodeScannerState createState() => _CodeScannerState();
}

class _CodeScannerState extends State<CodeScanner> {
  final MobileScannerController cameraController = MobileScannerController();
  String scannedData = '';

  Future<List<Map<String, dynamic>>> _queryData(String code) async {
    final allRows = await dbHelper.queryAllRows();
    debugPrint('query all rows:');
    for (final row in allRows) {
      debugPrint(row.toString());
    }
    return allRows;
  }

  Future<bool> _isCodeScanned(String code) async {
    final allRows = await dbHelper.queryAllRows();
    for (final row in allRows) {
      if (row[DatabaseHelper.columnData] == code) {
        return true;
      }
    }
    return false;
  }

  void _showScannedData(BuildContext context, String code) async {
    setState(() {
      scannedData = 'BARCODE: $code';

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('ข้อมูลที่สแกน'),
            content: Text(scannedData),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.pop(context); // Close the dialog
                },
                child: const Text('ยกเลิก'),
              ),
              TextButton(
                onPressed: () async {
                  Navigator.pop(context); // Close the dialog
                  List<Map<String, dynamic>> queryData = await _queryData(code);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CardDetailPage(
                        cardName: widget.cardName,
                        queryData: queryData,
                      ),
                    ),
                  );
                },
                child: const Text('เพิ่มหนังสือ'),
              ),
            ],
          );
        },
      );
    });
  }

  void _showOptionsDialog(BuildContext context) async {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('ตัวเลือกเพิ่มเติม'),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              // ListTile(
              //   leading: Icon(Icons.drive_file_move),
              //   title: const Text('Add'),
              //   onTap: () {
              //     // Handle add option
              //     Navigator.of(context).pop();
              //   },
              // ),
              // ListTile(
              //   leading: Icon(Icons.file_copy),
              //   title: const Text('Display Shelf'),
              //   onTap: () async {
              //     // Handle display shelf option
              //     Navigator.of(context).pop();
              //     final queryData = await _queryData(scannedData);
              //     if (queryData.isNotEmpty) {
              //       Navigator.push(
              //         context,
              //         MaterialPageRoute(
              //           builder: (context) => CardDetailPage(
              //             cardName: widget.cardName,
              //             queryData: queryData,
              //           ),
              //         ),
              //       );
              //     } else {
              //       showDialog(
              //         context: context,
              //         builder: (BuildContext context) {
              //           return AlertDialog(
              //             title: const Text('ไม่พบข้อมูล'),
              //             content: const Text(
              //               'ไม่พบข้อมูลที่ตรงกันสำหรับรหัสที่สแกน.',
              //             ),
              //             actions: [
              //               TextButton(
              //                 onPressed: () {
              //                   Navigator.of(context).pop();
              //                 },
              //                 child: const Text('OK'),
              //               ),
              //             ],
              //           );
              //         },
              //       );
              //     }
              //   },
              // ),
              ListTile(
                leading: Icon(Icons.delete),
                title: const Text('ลบ'),
                onTap: () {
                  // Handle delete option
                  Navigator.of(context).pop();
                  _deleteAllData(); // Call the _deleteAllData() method to delete all data
                },
              ),
            ],
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('ปิด'),
            ),
          ],
        );
      },
    );
  }

  void _deleteAllData() async {
    await dbHelper.deleteAllRows();
    debugPrint('deleted all rows');

    // Update the UI by triggering a rebuild
    setState(() {
      scannedData = ''; // Clear the scanned data
    });
  }

  void _insert(String code, String cardName) async {
    Map<String, dynamic> row = {
      DatabaseHelper.columnName: cardName,
      DatabaseHelper.columnData: code,
      DatabaseHelper.columnDateTime:
          DateFormat("yyyy-MM-dd HH:mm:ss").format(DateTime.now()),
    };
    final id = await dbHelper.insert(row);
    debugPrint('inserted row id: $id');
  }

  void _update(String code) async {
    // row to update
    Map<String, dynamic> row = {
      DatabaseHelper.columnId: 1,
      DatabaseHelper.columnName: 'newscanned_data',
      DatabaseHelper.columnData: code,
    };
    final rowsAffected = await dbHelper.update(row);
    debugPrint('updated $rowsAffected row(s)');
  }

  void _delete() async {
    // Assuming that the number of rows is the id for the last row.
    final id = await dbHelper.queryRowCount();
    final rowsDeleted = await dbHelper.delete(id);
    debugPrint('deleted $rowsDeleted row(s): row $id');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('สแกนหนังสือ'),
        actions: [
          IconButton(
            color: Colors.white,
            icon: ValueListenableBuilder(
              valueListenable: cameraController.torchState,
              builder: (context, state, child) {
                switch (state as TorchState) {
                  case TorchState.off:
                    return const Icon(Icons.flash_off, color: Colors.grey);
                  case TorchState.on:
                    return const Icon(Icons.flash_on, color: Colors.yellow);
                }
              },
            ),
            iconSize: 32.0,
            onPressed: () => cameraController.toggleTorch(),
          ),
          IconButton(
            color: Colors.white,
            icon: ValueListenableBuilder(
              valueListenable: cameraController.cameraFacingState,
              builder: (context, state, child) {
                switch (state as CameraFacing) {
                  case CameraFacing.front:
                    return const Icon(Icons.camera_front);
                  case CameraFacing.back:
                    return const Icon(Icons.camera_rear);
                }
              },
            ),
            iconSize: 32.0,
            onPressed: () => cameraController.switchCamera(),
          ),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: Stack(
              children: [
                MobileScanner(
                  allowDuplicates: false,
                  controller: cameraController,
                  onDetect: (barcode, args) async {
                    if (barcode.rawValue == null) {
                      debugPrint('สแกนบาร์โค้ดไม่สำเร็จ');
                    } else {
                      final String code = barcode.rawValue!;
                      debugPrint('พบบาร์โค้ดแล้ว! $code');
                      final bool isScanned = await _isCodeScanned(code);
                      if (isScanned) {
                        debugPrint('สแกนบาร์โค้ดแล้ว');
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: const Text('หนังสือซ้ำกัน'),
                              content: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  const SizedBox(height: 8.0),
                                  Text(
                                      'หนังสือที่มีบาร์โค้ด: $code มีอยู่แล้ว'),
                                ],
                              ),
                              actions: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: const Text('ยกเลิก'),
                                ),
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    _showOptionsDialog(
                                        context); // Show the options dialog
                                  },
                                  child: const Text('เพิ่มเติม'),
                                ),
                              ],
                            );
                          },
                        );
                      } else {
                        _showScannedData(
                          context,
                          code,
                        ); // Show the scanned data
                        _insert(
                          code,
                          widget.cardName,
                        ); // Insert the scanned data into the database
                      }
                    }
                  },
                ),
                QRScannerOverlay(overlayColour: Colors.black.withOpacity(0.5)),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(scannedData),
          ),
        ],
      ),
    );
  }
}
