import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:csv/csv.dart';
import 'package:provider/provider.dart';
import 'package:share_plus/share_plus.dart';
import '../main.dart';
import 'UserProfile.dart';
// import 'sidemenu.dart';

class ConvertToCsv extends StatefulWidget {
  const ConvertToCsv({Key? key}) : super(key: key);

  @override
  _ConvertToCsvState createState() => _ConvertToCsvState();
}

class _ConvertToCsvState extends State<ConvertToCsv> {
  List<List<dynamic>>? _csvData;
  bool _isLeftAligned = false;
  bool _isAscending = true;

  @override
  void initState() {
    super.initState();
    loadQueryData();
  }

  Future<void> loadQueryData() async {
    try {
      final dataList = await dbHelper.queryAllRows();
      final userProfileModel =
          Provider.of<UserProfileModel>(context, listen: false);
      final List<List<dynamic>> rows = dataList
          .map((e) => [
                e['id'],
                e['code'],
                e['data'],
                e['datetime'],
                userProfileModel.name,
                userProfileModel.location,
              ])
          .toList();
      final csvString = const ListToCsvConverter().convert(rows);
      final directory = await getTemporaryDirectory();
      final filePath = '${directory.path}/data.csv';
      final file = File(filePath);
      await file.writeAsString(csvString);
      print('CSV file saved to $filePath');

      final csvData = await file.readAsString();
      setState(() {
        _csvData = CsvToListConverter().convert(csvData);
      });
    } catch (e) {
      print('Error loading CSV file: $e');
    }
  }

  Future<void> refreshData() async {
    await loadQueryData();
  }

  @override
  Widget build(BuildContext context) {
    if (_csvData == null) {
      return const Center(child: CircularProgressIndicator());
    }
    return Scaffold(
      // drawer: SideMenu(),
      appBar: AppBar(
        title: const Text('ข้อมูลหนังสือที่สแกน'),
        actions: [
          IconButton(
            icon: const Icon(Icons.share),
            onPressed: _shareCsvData,
          ),
          IconButton(
            icon: const Icon(Icons.delete),
            onPressed: _clearData,
          ),
          IconButton(
            icon: Icon(_isLeftAligned
                ? Icons.import_export
                : Icons.import_export_outlined),
            onPressed: _toggleAlignment,
          ),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: refreshData,
        child: Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: ListView.builder(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            itemCount: _csvData!.length,
            itemBuilder: (BuildContext context, int index) {
              final row = _csvData![index];
              return Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(color: Colors.grey.shade300),
                  ),
                ),
                child: Card(
                  elevation: 4.0,
                  margin: const EdgeInsets.only(bottom: 16.0),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Stack(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('ID: ${row[0]}',
                                style: TextStyle(fontWeight: FontWeight.bold)),
                            const SizedBox(height: 8.0),
                            Text('Collectionname: ${row[1]}'),
                            const SizedBox(height: 8.0),
                            Text('Data: ${row[2]}'),
                            const SizedBox(height: 8.0),
                            Text('Datetime: ${row[3]}'),
                            const SizedBox(height: 8.0),
                            Consumer<UserProfileModel>(
                              builder: (context, userProfile, child) {
                                return Align(
                                  alignment: Alignment.centerLeft,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text('Scanned By: ${userProfile.name}'),
                                      const SizedBox(height: 8.0),
                                      Text('Location: ${userProfile.location}')
                                    ],
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                        Positioned(
                          top: 0,
                          right: 0,
                          child: IconButton(
                            icon: Icon(Icons.delete),
                            onPressed: () {
                              _deleteCard(index);
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Future<void> _shareCsvData() async {
    try {
      final List<List<dynamic>> rowsWithHeader = [
        ['ID', 'Collectionname', 'Data', 'Datetime', 'ScanBy', 'Location'],
        ..._csvData!,
      ];
      final csvData = _convertToCsv(rowsWithHeader);
      final tempDir = await getTemporaryDirectory();
      final tempFile = File('${tempDir.path}/my_data.csv');
      await tempFile.writeAsString(csvData);
      final result = await Share.shareFiles(
        [tempFile.path],
        mimeTypes: ['text/csv'],
      );
    } catch (e) {
      print('Error sharing CSV data: $e');
    }
  }

  String _convertToCsv(List<List<dynamic>> rows) {
    return const ListToCsvConverter().convert(rows);
  }

  void _clearData() async {
    await dbHelper.deleteAllRows(); // Delete all rows from the database
    setState(() {
      _csvData = []; // Clear the data from the view
    });
    final snackBar = SnackBar(content: const Text('Data cleared successfully'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void _updateData() async {
    setState(() {
      _csvData = null; // Set the data to null to display a loading indicator
    });

    // Load the updated query data
    await loadQueryData();
  }

  void _toggleAlignment() {
    setState(() {
      _isLeftAligned = !_isLeftAligned;
      _isAscending = !_isAscending;
      _sortData();
    });
  }

  void _sortData() {
    _csvData!.sort((a, b) {
      final datetimeA = DateTime.parse(a[3]);
      final datetimeB = DateTime.parse(b[3]);
      return _isAscending
          ? datetimeA.compareTo(datetimeB)
          : datetimeB.compareTo(datetimeA);
    });
  }

  void _deleteCard(int index) async {
    // Extract ID from the card data
    int id = _csvData![index][0];

    // Delete the row from the database
    await dbHelper.delete(id);

    // Remove the corresponding data from the view
    setState(() {
      _csvData!.removeAt(index);
    });

    final snackBar = SnackBar(content: const Text('Card deleted successfully'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
