import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';

import 'About.dart';
import 'UserProfile.dart';
import 'convertToCsv.dart';
import 'home.dart';
import 'settings.dart';

class FabTabs extends StatefulWidget {
  int selectedIndex = 0;
  FabTabs({required this.selectedIndex});

  @override
  State<FabTabs> createState() => _FabTabsState();
}

class _FabTabsState extends State<FabTabs> {
  int currentIndex = 0;

  void onItemTapped(int index) {
    setState(() {
      currentIndex = index;
      widget.selectedIndex = index;
    });
  }

  final List<Widget> pages = [
    Home(),
    ConvertToCsv(),
    UserProfile(),
    // Settings(),
    AboutMe(),
  ];

  @override
  void initState() {
    currentIndex = widget.selectedIndex;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: currentIndex,
        children: pages,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: CurvedNavigationBar(
        index: currentIndex,
        height: 60,
        backgroundColor: Colors.white,
        color: Colors.pink,
        buttonBackgroundColor: Colors.pink,
        onTap: onItemTapped,
        items: <Widget>[
          Icon(
            Icons.home_filled,
            size: 30,
            color: Colors.white,
          ),
          Icon(
            Icons.assignment_outlined,
            size: 30,
            color: Colors.white,
          ),
          Icon(
            Icons.account_circle,
            size: 30,
            color: Colors.white,
          ),
          Icon(
            Icons.error_outline,
            size: 30,
            color: Colors.white,
          ),
        ],
      ),
    );
  }
}
