import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

// import 'sidemenu.dart';

class UserProfile extends StatelessWidget {
  const UserProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var userProfileModel = Provider.of<UserProfileModel>(context);

    return Scaffold(
      // drawer: SideMenu(),
      appBar: AppBar(
        title: Text("ข้อมูลผู้ใช้"),
        backgroundColor: Colors.pinkAccent,
        actions: [
          IconButton(
            icon: Icon(Icons.edit),
            onPressed: () {
              userProfileModel.editProfile(context);
            },
          ),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: userProfileModel.loadProfileData,
        child: ListView(
          physics: AlwaysScrollableScrollPhysics(),
          children: [
            Container(
              alignment: Alignment.topCenter,
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 40),
              child: SingleChildScrollView(
                child: Card(
                  elevation: 6,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(20),
                    child: Column(
                      children: [
                        Container(
                          height: 200,
                          width: 200,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black.withOpacity(0.3),
                                blurRadius: 10,
                                offset: Offset(0, 3),
                              ),
                            ],
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(100),
                            child: Image.network(
                              "https://cdn.icon-icons.com/icons2/928/PNG/512/user_icon-icons.com_72180.png",
                              height: 100,
                              width: 100,
                            ),
                          ),
                        ),
                        SizedBox(height: 20),
                        Text('Name : ${userProfileModel.name}'),
                        // SizedBox(height: 20),
                        // Text('Email : ${userProfileModel.email}'),
                        SizedBox(height: 20),
                        Text('Location : ${userProfileModel.location}'),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class UserProfileModel with ChangeNotifier {
  String name;
  String email;
  String location;

  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _locationController = TextEditingController();

  UserProfileModel({
    this.name = 'John',
    this.email = 'john@example.com',
    this.location = 'BUU Library (BangSaen)',
  }) {
    loadProfileData();
  }

  Future<void> loadProfileData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    name = prefs.getString('name') ?? 'John';
    email = prefs.getString('email') ?? 'john@example.com';
    location = prefs.getString('location') ?? 'BUU Library (BangSaen)';
    notifyListeners();
  }

  Future<void> editProfile(BuildContext context) async {
    _nameController.text = name;
    _emailController.text = email;
    _locationController.text = location;

    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('แก้ไขโปรไฟล์'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                TextFormField(
                  controller: _nameController,
                  decoration: const InputDecoration(
                    labelText: 'Name',
                  ),
                ),
                // TextFormField(
                //   controller: _emailController,
                //   decoration: const InputDecoration(
                //     labelText: 'Email',
                //   ),
                // ),
                TextFormField(
                  controller: _locationController,
                  decoration: const InputDecoration(
                    labelText: 'Location',
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('ยกเลิก'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text('ตกลง'),
              onPressed: () async {
                await updateProfile(
                  _nameController.text,
                  _emailController.text,
                  _locationController.text,
                );
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> updateProfile(String name, String email, String location) async {
    this.name = name;
    this.email = email;
    this.location = location;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('name', name);
    await prefs.setString('email', email);
    await prefs.setString('location', location);
    notifyListeners();
  }
}
