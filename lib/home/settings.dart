import 'package:flutter/material.dart';

import '../main.dart';
// import 'sidemenu.dart';

class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  Future<void> _deleteAllScannedData(BuildContext context) async {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('ยืนยันการลบ'),
          content: Text('คุณแน่ใจหรือไม่ว่าต้องการลบข้อมูลที่สแกนทั้งหมด'),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('ยกเลิก'),
            ),
            TextButton(
              onPressed: () async {
                Navigator.of(context).pop(); // Close the confirmation dialog

                final rowsDeleted = await dbHelper.deleteAllRows();
                print('Rows deleted: $rowsDeleted');

                if (rowsDeleted > 0) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text('ลบข้อมูลที่สแกนทั้งหมดแล้ว'),
                    ),
                  );
                } else {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('ไม่มีข้อมูล'),
                        content: Text('ข้อมูลของคุณว่างเปล่าแล้ว'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('ตกลง'),
                          ),
                        ],
                      );
                    },
                  );
                }
              },
              child: Text('ลบ'),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // drawer: SideMenu(),
      appBar: AppBar(
        title: Text("ลบหนังสือทั้งหมด"),
        backgroundColor: Colors.pinkAccent,
      ),
      body: Center(
        child: ListView.builder(
          padding: EdgeInsets.all(8.0),
          itemCount: 1,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black, // Set the border color here
                  width: 2.0, // Set the border width here
                ),
              ),
              child: Card(
                child: InkWell(
                  onTap: () => _deleteAllScannedData(context),
                  child: Padding(
                    padding: const EdgeInsets.all(30.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.delete_forever,
                          size: 50,
                          color: Colors.red,
                        ),
                        const SizedBox(height: 10),
                        const Text('ลบข้อมูลที่สแกน'),
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
