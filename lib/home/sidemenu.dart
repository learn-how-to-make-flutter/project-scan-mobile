import 'package:flutter/material.dart';

import 'FabTabs.dart';

class SideMenu extends StatefulWidget {
  @override
  State<SideMenu> createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Colors.white,
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(1.0),
                bottomRight: Radius.circular(1.0),
              ),
              color: Colors.pinkAccent,
            ),
            child: Row(
              children: [
                Container(
                  width: 100, // Set the desired width
                  height: 100, // Set the desired height
                  child: Image.network(
                    "https://s.isanook.com/ca/0/rp/r/w728/ya0xa0m1w0/aHR0cHM6Ly9zLmlzYW5vb2suY29tL2NhLzAvdWQvMjc5LzEzOTkzMjMvYnV1LWxvZ28xMV8xLnBuZw==.png",
                    fit: BoxFit.contain, // Adjust the fit as needed
                  ),
                ),
                SizedBox(
                    width: 10), // Adjust the spacing between the image and text
                Text(
                  'หนังสือ ห้องสมุด',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
          ListTile(
            leading: Icon(Icons.home_filled),
            title: Text("หน้าหลัก"),
            onTap: () {
              Navigator.of(context).pop(
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        FabTabs(selectedIndex: 0)),
              );
            },
          ),
          // ListTile(
          //   leading: Icon(Icons.more_horiz_outlined),
          //   title: Text("More"),
          //   onTap: () => {
          //     Navigator.pushReplacement(
          //         context,
          //         MaterialPageRoute(
          //             builder: (context) => FabTabs(selectedIndex: 1)))
          //   },
          // ),
          // ListTile(
          //   leading: Icon(Icons.person_pin_outlined),
          //   title: Text("Team"),
          //   onTap: () => {
          //     Navigator.pushReplacement(
          //         context,
          //         MaterialPageRoute(
          //             builder: (context) => FabTabs(selectedIndex: 2)))
          //   },
          // ),
          // ListTile(
          //   leading: Icon(Icons.settings),
          //   title: Text("การตั้งค่า"),
          //   onTap: () => {
          //     Navigator.pushReplacement(
          //         context,
          //         MaterialPageRoute(
          //             builder: (context) => FabTabs(selectedIndex: 3)))
          //   },
          // ),
        ],
      ),
    );
  }
}
