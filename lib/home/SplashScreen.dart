import 'dart:async';

import 'package:flutter/material.dart';

import 'FabTabs.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(Duration(seconds: 5), () {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (_) => FabTabs(selectedIndex: 0)));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [Colors.blue, Colors.purple],
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
          ), // LinearGradient
        ), // BoxDecoration
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.network(
              "https://cdn-icons-png.flaticon.com/512/3897/3897998.png",
              width: MediaQuery.of(context).size.width *
                  0.5, // Adjust the scaling factor as needed
              height: MediaQuery.of(context).size.height *
                  0.3, // Adjust the scaling factor as needed
            ),
            SizedBox(height: 20),
            Center(
              child: Text(
                'ระบบตรวจสอบ \n นับจำนวนหนังสือ',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 30,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
