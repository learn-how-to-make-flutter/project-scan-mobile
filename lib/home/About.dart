import 'package:flutter/material.dart';

class AboutMe extends StatefulWidget {
  const AboutMe({Key? key}) : super(key: key);

  @override
  State<AboutMe> createState() => _AboutMeState();
}

class _AboutMeState extends State<AboutMe> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // drawer: SideMenu(),
      appBar: AppBar(
        title: Text("About Us"),
        backgroundColor: Colors.pinkAccent,
      ),
      body: SingleChildScrollView(
        // Added SingleChildScrollView for preventing overflow
        child: Padding(
          // Added Padding for better UI experience
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Image.asset(
                "assets/play_store_512.png",
                height: 200,
                width: 150,
              ),
              SizedBox(height: 20),
              Text(
                "Scan Books",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 20),
              Text(
                "V.1.0.0",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 20),
              Text(
                "เกี่ยวกับแอพ",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 20),
              Text(
                "แอปนี้เป็นเครื่องมือที่มีประโยชน์ที่ช่วยให้ทุกคนจัดการห้องสมุดส่วนตัวขนาดเล็กได้ ขอให้สนุกกับการใช้ Scan Books และแจ้งให้เราทราบความคิดเห็นของคุณเพื่อปรับปรุงประสิทธิภาพของแอป",
                style: TextStyle(
                  fontSize: 16,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20),
              Divider(
                color: Colors.black,
                thickness: 2,
              ),
              SizedBox(height: 20),
              Text(
                "Created by",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 20),
              Text(
                "Nithiphat Likhitwattanakit \n Burapha University ",
                style: TextStyle(
                  fontSize: 20,
                ),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
