import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';

import '../Scan/code_scanner.dart';
import '../db/db_helper.dart';
import '../main.dart';
import 'FabTabs.dart';
import 'home.dart';

class CardDetailPage extends StatefulWidget {
  final String cardName;

  const CardDetailPage({
    Key? key,
    required this.cardName,
    required List queryData,
  }) : super(key: key);

  @override
  _CardDetailPageState createState() => _CardDetailPageState();
}

class _CardDetailPageState extends State<CardDetailPage> {
  int _currentIndex = 0;

  Future<List<Map<String, dynamic>>> _getScannedDataList() async {
    final allRows = await dbHelper.queryAllRows();
    final filteredRows = allRows
        .where((row) => row[DatabaseHelper.columnName] == widget.cardName)
        .toList();
    return filteredRows;
  }

  void _onTabTapped(int index) async {
    setState(() {
      _currentIndex = index;
    });

    if (index == 0) {
      List<Map<String, dynamic>> result = await Navigator.of(context).push(
        MaterialPageRoute(
          builder: (BuildContext context) =>
              CodeScanner(cardName: widget.cardName),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.cardName),
        actions: [
          IconButton(
            icon: Icon(Icons.home),
            onPressed: () {
              Navigator.pop(
                context,
                MaterialPageRoute(
                  builder: (context) => Home(),
                ),
              );
            },
          ),
        ],
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(
              context,
              MaterialPageRoute(
                builder: (context) => FabTabs(selectedIndex: 0),
              ),
            );
          },
        ),
      ),
      body: FutureBuilder<List<Map<String, dynamic>>>(
        future: _getScannedDataList(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final _scannedDataList = snapshot.data!;
            if (_scannedDataList.isEmpty) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.network(
                      'https://shorturl.asia/zxLXl', // replace with your image URL
                      height: 250,
                      width: 220,
                    ),
                    SizedBox(
                        height:
                            10), // to add a space between the image and the text
                    Text(
                      'ไม่ข้อมูลของหนังสือ',
                      style: TextStyle(fontSize: 20),
                    ),
                  ],
                ),
              );
            } else {
              return ListView.builder(
                itemCount: _scannedDataList.length,
                itemBuilder: (BuildContext context, int index) {
                  final row = _scannedDataList[index];
                  final id = row[DatabaseHelper.columnId];
                  final name = row[DatabaseHelper.columnName];
                  final data = row[DatabaseHelper.columnData];
                  final datetime = row[DatabaseHelper.columnDateTime];

                  // Continue with your existing code...
                  return Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ListTile(
                            leading: Image.network(
                              'https://cdn-icons-png.flaticon.com/512/5318/5318847.png',
                              height: 70,
                              width: 70,
                            ),
                            title: Text('ID: $id'),
                            subtitle: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '$name',
                                  style: TextStyle(
                                      fontSize: 15, color: Colors.black),
                                ),
                                Text(
                                  'code: $data',
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.black,
                                  ),
                                ),
                                Text(
                                  'datetime: $datetime ',
                                  style: TextStyle(
                                    fontSize: 13,
                                    color: Colors.black,
                                  ),
                                ),
                              ],
                            ),
                            trailing: IconButton(
                              icon: Icon(Icons.delete),
                              onPressed: () async {
                                print("การลบรายการด้วย ID $id...");
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: Text('ลบรายการ'),
                                      content: Text(
                                          'คุณแน่ใจหรือว่าต้องการลบรายการนี้หรือไม่?'),
                                      actions: [
                                        TextButton(
                                          child: Text('ยกเลิก'),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                        TextButton(
                                          child: Text('ลบ'),
                                          onPressed: () async {
                                            final rowsDeleted =
                                                await dbHelper.delete(id);
                                            print("Rows deleted: $rowsDeleted");
                                            if (rowsDeleted > 0) {
                                              // Rebuild the widget to reflect the new state
                                              ScaffoldMessenger.of(context)
                                                  .showSnackBar(
                                                SnackBar(
                                                  content: Text('ลบรายการแล้ว'),
                                                ),
                                              );
                                              Navigator.pop(context);
                                              Navigator.pushReplacement(
                                                context,
                                                MaterialPageRoute(
                                                  builder:
                                                      (BuildContext context) =>
                                                          CardDetailPage(
                                                    cardName: widget.cardName,
                                                    queryData: [],
                                                  ),
                                                ),
                                              );
                                            }
                                          },
                                        ),
                                      ],
                                    );
                                  },
                                );
                              },
                            ),
                            onTap: () {
                              // Handle item tap
                            },
                          ),
                        ],
                      ),
                    ),
                  );
                },
              );
            }
          } else if (snapshot.hasError) {
            return Center(
              child: Text('Error: ${snapshot.error}'),
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
      bottomNavigationBar: CurvedNavigationBar(
        index: _currentIndex,
        height: 50,
        backgroundColor: Colors.white,
        color: Colors.pink,
        buttonBackgroundColor: Colors.pink,
        items: <Widget>[
          GestureDetector(
            onTap: () {
              _onTabTapped(0);
            },
            child: Center(
              child: Icon(
                Icons.qr_code_scanner_rounded,
                size: 30,
                color: Colors.white,
              ),
            ),
          ),
        ],
        onTap: _onTabTapped,
      ),
    );
  }
}
