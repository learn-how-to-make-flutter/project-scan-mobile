import 'package:flutter/material.dart';

import '../db/db_helper.dart';
import 'CardDetailPage.dart';
// import 'sidemenu.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<String> cardList = []; // List to store the card names
  List<int> cardDataCount = []; // List to store the data counts
  TextEditingController searchController = TextEditingController();
  bool isSearching = false;
  List<String> filteredCardList = [];

  @override
  void initState() {
    super.initState();
    // Create the default cards
    addCard("ชั้นวาง A");
    addCard("ชั้นวาง B");
    addCard("ชั้นวาง C");
  }

  Future<void> updateDataCounts() async {
    List<int> updatedDataCounts = [];

    for (String name in cardList) {
      int count = await getDataCount(name);
      updatedDataCounts.add(count);
    }

    setState(() {
      cardDataCount = updatedDataCounts;
    });
  }

  void addCard(String name) {
    if (isNameDuplicate(name)) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('ชื่อซ้ำ'),
            content: Text('มีชื่อเดียวกันอยู่แล้ว'),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('ตกลง'),
              ),
            ],
          );
        },
      );
    } else {
      setState(() {
        // Add the card name to the list
        cardList.add(name);
      });
      // Retrieve the data count and update the cardDataCount list
      getDataCount(name).then((count) {
        setState(() {
          cardDataCount.add(count);
        });
      });
    }
  }

  bool isNameDuplicate(String name) {
    return cardList.contains(name);
  }

  void navigateToCardDetailPage(String cardName) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => CardDetailPage(
          cardName: cardName,
          queryData: [],
        ),
      ),
    );
  }

  Future<int> getDataCount(String name) async {
    int count = 0;
    DatabaseHelper databaseHelper = DatabaseHelper();
    await databaseHelper.init();
    List<Map<String, dynamic>> rows = await databaseHelper.queryAllRows();
    count = rows.where((row) => row[DatabaseHelper.columnName] == name).length;
    return count;
  }

  Widget createDataCard(String name) {
    int dataCount = cardDataCount[cardList.indexOf(name)];

    return GestureDetector(
      onTap: () {
        navigateToCardDetailPage(name);
      },
      onLongPress: () => showCardOptions(name),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12.0),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.2),
              blurRadius: 6.0,
              offset: Offset(0, 3),
            ),
          ],
        ),
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                padding: EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.network(
                      "https://cdn.icon-icons.com/icons2/1001/PNG/512/ilustracoes_04-10_icon-icons.com_75464.png",
                      width: 60,
                      height: 60,
                    ),
                    SizedBox(height: 8.0),
                    Text(
                      name,
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Text(
              '$dataCount หนังสือ',
              style: TextStyle(
                fontSize: 14.0,
                color: Colors.black,
              ),
            ),
            SizedBox(height: 5.0),
          ],
        ),
      ),
    );
  }

  void showFormDialog() {
    String name = '';
    bool showWarning = false;
    String selectedCard = cardList.isNotEmpty ? cardList[0] : '';
    TextEditingController nameController = TextEditingController();

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
            return AlertDialog(
              title: Text('ชั้นวางใหม่'),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(
                        'ชื่อ:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(width: 8.0),
                      Expanded(
                        child: TextField(
                          controller: nameController,
                          onChanged: (value) {
                            setState(() {
                              name = value;
                              showWarning = false; // Reset the warning
                            });
                          },
                          decoration: InputDecoration(
                            hintText: 'ชั้นวางใหม่',
                          ),
                        ),
                      ),
                    ],
                  ),
                  if (showWarning)
                    Text(
                      'ต้องระบุชื่อ',
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                ],
              ),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('ยกเลิก'),
                ),
                ElevatedButton(
                  onPressed: () {
                    if (name.isEmpty) {
                      setState(() {
                        showWarning = true;
                      });
                    } else {
                      Navigator.of(context).pop();
                      addCard(name);
                      nameController.clear(); // Clear the text field
                    }
                  },
                  child: Text('ตกลง'),
                ),
              ],
            );
          },
        );
      },
    );
  }

  void showCardOptions(String name) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('แก้ไขชั้นวางหนังสือ'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListTile(
                leading: Icon(Icons.edit),
                title: Text('เปลี่ยนชื่อ'),
                onTap: () {
                  Navigator.of(context).pop();
                  showEditNameDialog(name);
                },
              ),
              ListTile(
                leading: Icon(Icons.delete),
                title: Text('ลบ'),
                onTap: () {
                  Navigator.of(context).pop();
                  deleteCard(name);
                },
              ),
            ],
          ),
        );
      },
    );
  }

  void showEditNameDialog(String currentName) {
    String newName = currentName;
    bool showWarning = false;

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
            return AlertDialog(
              title: Text('เปลี่ยนชื่อ'),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(
                        'ชื่อ:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(width: 8.0),
                      Expanded(
                        child: TextField(
                          onChanged: (value) {
                            setState(() {
                              newName = value;
                              showWarning = false; // Reset the warning
                            });
                          },
                          decoration: InputDecoration(
                            hintText: 'ชั้นวางใหม่',
                            errorText:
                                showWarning ? 'ชื่อซ้ำหรือไม่ถูกต้อง' : null,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('ยกเลิก'),
                ),
                ElevatedButton(
                  onPressed: () {
                    if (newName.isEmpty || isNameDuplicate(newName)) {
                      setState(() {
                        showWarning = true;
                      });
                    } else {
                      Navigator.of(context).pop();
                      renameCard(currentName, newName);
                    }
                  },
                  child: Text('ตกลง'),
                ),
              ],
            );
          },
        );
      },
    );
  }

  void deleteCard(String name) {
    setState(() {
      int index = cardList.indexOf(name);
      if (index != -1) {
        cardList.removeAt(index);
        cardDataCount.removeAt(index);
      }
    });
  }

  void renameCard(String currentName, String newName) {
    setState(() {
      int index = cardList.indexOf(currentName);
      if (index != -1) {
        cardList[index] = newName;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    List<String> filteredCardList = cardList.where((card) {
      final query = searchController.text.toLowerCase();
      return card.toLowerCase().contains(query);
    }).toList();

    return Scaffold(
      // drawer: SideMenu(),
      appBar: AppBar(
        title: isSearching
            ? TextField(
                controller: searchController,
                onChanged: (value) {
                  setState(() {
                    filteredCardList = cardList
                        .where((card) =>
                            card.toLowerCase().contains(value.toLowerCase()))
                        .toList();
                  });
                },
                decoration: InputDecoration(
                  hintText: 'Search',
                  hintStyle: TextStyle(color: Colors.white),
                  border: InputBorder.none,
                ),
                style: TextStyle(color: Colors.white),
              )
            : Text('ชั้นวางหนังสือ'),
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              setState(() {
                isSearching = !isSearching;
                if (!isSearching) {
                  searchController.clear();
                }
              });
            },
          ),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: updateDataCounts, // Set the refresh callback
        child: GridView.builder(
          physics: AlwaysScrollableScrollPhysics(),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            childAspectRatio: 0.76,
            crossAxisSpacing: 20,
            mainAxisSpacing: 20,
          ),
          padding: EdgeInsets.all(8.0),
          itemCount: filteredCardList.length,
          itemBuilder: (context, index) {
            return createDataCard(filteredCardList[index]);
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: showFormDialog,
        child: Icon(Icons.add),
      ),
    );
  }
}
