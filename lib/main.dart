import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'db/db_helper.dart';
import 'home/SplashScreen.dart';
import 'home/UserProfile.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await dbHelper.init();
  runApp(
    ChangeNotifierProvider(
      create: (context) => UserProfileModel(),
      child: const MyApp(),
    ),
  );
}

final dbHelper = DatabaseHelper();

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.pink,
      ),
      home: SplashScreen(),
    );
  }
}
